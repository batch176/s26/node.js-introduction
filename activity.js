let http = require("http");

http.createServer(function(request,response){
	
	console.log(request.url);
	
	if(request.url === "/"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to B176 Booking System');
	} else if(request.url === "/courses"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to the Course page. View our Courses!');
	}
	else if(request.url === "/profile"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to your profile. View your details');
	} else {
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end('Resources cannot be found.');
	}

	

}).listen(5000);